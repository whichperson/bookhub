@extends('layouts.app')

@section('content')
    <div id="register" class="h-100">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-10 d-flex" id="main-content">
                    <div class="col-md-6 overlay">
                        <div class="h-100 flex-column justify-content-center align-items-center d-flex">
                            <h1 class="title">Welcome back!</h1>
                            <p class="subtitle w-75 mt-2">Please log in with your account credentials.</p>
                            <a href="{{ route('login') }}" class="btn btn-normal mt-2">sign in</a>
                        </div>
                    </div>
                    <div class="col-md-6" id="register-form">
                        <div class="inputs text-capitalize d-flex flex-column justify-content-center align-items-center h-100">
                            <h1 class="title mb-3">Sign Up</h1>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        account_circle
                                    </i>
                                    <input name="username" id="username" aria-label="Username" type="text" required
                                           class="input-field @error('username') is-invalid @enderror"
                                           value="{{ old('username') }}" autocomplete="username">
                                    <label for="username">Username</label>

                                    @error('username')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        email
                                    </i>
                                    <input name="email" id="email" aria-label="Email" type="email" required
                                           class="input-field validate @error('email') is-invalid @enderror"
                                           value="{{ old('email') }}" autocomplete="email">
                                    <label for="email">Email</label>

                                    @error('email')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        lock
                                    </i>
                                    <input name="password" id="password" aria-label="Password" type="password" required
                                           class="input-field validate password-field @error('password') is-invalid @enderror"
                                           autocomplete="new-password"
                                           minlength="8">
                                    <label for="password">Password</label>

                                    <div class="requirements text-muted"><small>Minimum 8 characters</small>
                                    </div>
                                    <i class="material-icons showPassword">
                                        visibility
                                    </i>
                                    <i class="material-icons hidePassword">
                                        visibility_off
                                    </i>
                                    @error('password')
                                    {{ $message }}
                                    @enderror
                                </div>
                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        check
                                    </i>
                                    <input name="password_confirmation" id="password-confirm"
                                           aria-label="Confirm Password"
                                           type="password" required class="input-field" autocomplete="new-password">
                                    <label for="confirmPassword">Confirm Password</label>
                                </div>
                                <button type="submit" class="btn-form btn mx-auto d-flex text-center mt-5">SIGN UP
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endsection
