@extends('layouts.app')

@section('content')
    <div id="login" class="h-100">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-10 d-flex" id="main-content">
                    <div class="col-md-6" id="login-form">
                        <div class="inputs text-capitalize d-flex flex-column
                        justify-content-center align-items-center h-100">
                            <h1 class="title mb-3">Sign in</h1>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        email
                                    </i>
                                    <input name="email" id="email" aria-label="Email" type="email" required
                                           class="input-field @error('email') is-invalid @enderror"
                                           value="{{ old('email') }}"
                                           autocomplete="email">
                                    <label for="email">Email</label>
                                    @error('email')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group my-4 mx-auto w-75">
                                    <i class="material-icons-outlined prefix">
                                        lock
                                    </i>
                                    <input name="password" id="password" aria-label="Password" type="password" required
                                           class="input-field password-field @error('password') is-invalid @enderror"
                                           autocomplete="current-password">
                                    <label for="password">Password</label>
                                    <i class="material-icons showPassword">
                                        visibility
                                    </i>
                                    <i class="material-icons hidePassword">
                                        visibility_off
                                    </i>
                                    @error('password')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="helpers text-capitalize px-1 mb-4 w-75 mx-auto d-flex justify-content-between">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="remember"
                                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label text-sm" for="remember">Remember me</label>
                                    </div>
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}" class="text-sm">Forgot your password?</a>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-form mx-auto d-flex text-center mt-5">SIGN IN</button>

                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 overlay">
                        <div class="h-100 flex-column justify-content-center align-items-center d-flex">
                            <h1 class="title">Hello, there!</h1>
                            <p class="subtitle w-75 mt-2">Create an account and watch your library grow.</p>
                            <a href="{{ route('register') }}" class="btn btn-normal mt-2">sign up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endsection
