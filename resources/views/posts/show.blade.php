@extends('layouts.app')

@section('content')
    <div id="book">
        <div class="container h-100">
            <div class="row d-flex h-100 justify-content-center align-items-center">
                <div class="col-md-6">
                    <div class="book-cover">
                        <img src="{{env('AWS_URL').'/'.$post->image }}" alt="" class="w-100">
                    </div>
                </div>
                <div class="col-md-4">
                    <h1>{{ $post->title }}</h1>
                    <p>{{ $post->author }}</p>
                    <p>{{ $post->category }}</p>
                </div>
            </div>

        </div>
    </div>
@endsection
