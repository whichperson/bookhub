@extends('layouts.app')

@section('content')
    <div id="create" class="h-100">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-8">
                    <div id="main-content" class="p-5">
                        <h1 class="text-center mb-5">Add a Book</h1>
                        <form action="/p" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-10">
                                    <div class="form-group row">
                                        <input id="title" type="text"
                                               class="input-field @error('title') is-invalid @enderror"
                                               name="title" value="{{ old('title') }}" required autocomplete="title"
                                               autofocus>
                                        <label for="title">{{ __('Book Title') }}</label>
                                        @error('title')
                                        <strong>{{ $message }}</strong>
                                        @enderror
                                    </div>
                                    <div class="form-group row">
                                        <input id="author" type="text"
                                               class="input-field @error('author') is-invalid @enderror"
                                               name="author" value="{{ old('author') }}" required autocomplete="author"
                                               autofocus>
                                        <label for="author">{{ __('Book Author') }}</label>
                                        @error('author')
                                        <strong>{{ $message }}</strong>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="category">Book Category</label>
                                        <select id="category" class="form-control" required name="category">
                                            <option selected>Fiction</option>
                                            <option>Nonfiction</option>
                                            <option>Crime</option>
                                            <option>Horror</option>
                                            <option>Other</option>
                                        </select>

                                        @error('category')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="row">
                                        <label for="image">Book Cover</label>
                                        <input type="file" class="form-control-file" id="image" name="image">

                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <button type="submit" class="btn-form btn mx-auto d-flex text-center mt-5">ADD BOOK
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
