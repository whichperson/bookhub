@extends('layouts.app')

@section('content')
    <div id="index">
        <div class="container-fluid h-100">
            <div class="row h-100 px-2 justify-content-center">
                <!--Profile-->
                <div class="col-md-6 my-2" id="profile">

                    <div class="row px-3 pt-3 d-flex justify-content-end">
                        @can('update', $user->profile)
                            <edit-button style="display: block;"></edit-button>
                        @endcan
                    </div>

                    <div class="row d-flex align-items-center" id="header">
                        <div class="col-md-3">
                            <div class="profile-image mx-auto">
                                <img src="{{ $user->profile->profileImage() }}" class="rounded-circle w-100"
                                     alt="Profile image">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <h1>{{ '@'.$user->username}}</h1>
                            <p><span class="text-bold">{{ $postCount  }}</span> books</p>
                        </div>
                    </div>

                    <div class="row mt-5 px-4">
                        @if(Auth::check())
                            @if(Auth::user()->id == $user->id)
                                <greeting name="{{ $user->profile->name }}"></greeting>
                            @endif
                        @endif
                    </div>

                    <!--Stats-->
                    <div class="row px-4 mt-4">
                        <h2>Most Read Categories</h2>
                    </div>
                    <div class="row px-2 justify-content-center align-items-center mt-3" id="donut-chart">
                        <donut-chart user-id="{{ $user->id }}"></donut-chart>
                    </div>
                    <div class="row px-4 mt-4">
                        <h2>Favorite Authors</h2>
                    </div>
                    <div class="row px-2 justify-content-center align-items-center mt-3" id="bar-chart">
                        <bar-chart user-id="{{ $user->id }}"></bar-chart>
                    </div>
                </div>

                <!--Library-->
                <div class="col-md-6 my-2" id="library">
                    <div class="row px-3 pt-3 d-flex justify-content-end">
                        <a href="{{ route('logout') }}" class="log-out"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">
                                exit_to_app
                            </i>
                            <span class="tip">Log out</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    @if(Auth::check())
                        @if(Auth::user()->id == $user->id)
                            <search-bar></search-bar>
                        @endif
                    @endif

                    <div class="col-md-12">
                        <div class="row px-3 mt-5 d-flex justify-content-start align-items-center">
                            <h1>Library</h1>
                            @if(Auth::check())
                                @if(Auth::user()->id == $user->id)
                                    <a href="/p/create" class="ml-4 add-book"><i class="material-icons">
                                            add_circle_outline
                                        </i>
                                        <span class="tip">Add book</span></a>
                                @endif
                            @endif
                        </div>
                        <div class="row d-flex ">

                            @foreach($user->posts as $post)
                                <div class="col-md-4">
                                    <div class="cover">
                                        <a href="/p/{{ $post->id }}">
                                            <img src="{{ env('AWS_URL').'/'.$post->image }}" class="w-100"
                                                 alt="Book Cover">
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="edit-profile">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-6 text-center">
                    <div id="main-content" class="p-3">
                        <div class="row px-3 d-flex justify-content-end">
                            <cancel></cancel>
                        </div>
                        <h1>Edit Your Profile</h1>
                        <div class="row d-flex justify-content-center align-items-center">
                            <div class="col-md-10 mx-auto">
                                <form action="/profile/{{ $user->id }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                    <div class="row my-4 justify-content-center current-image">
                                        <div class="col-md-4">
                                            <img src="{{ $user->profile->profileImage() }}" class="rounded-circle w-100"
                                                 alt="">
                                        </div>
                                    </div>
                                    <div class="from-group row justify-content-center my-4">
                                        <div class="change-avatar">
                                            <button class="btn btn-normal">Change Avatar</button>
                                            <div style="opacity: 0;top: 0; position:absolute;left: 0;">
                                                <input type="file" class="input-field" id="image" name="image">
                                                <label for="image">Avatar</label>
                                            </div>
                                        </div>
                                        @if ($errors->has('image'))
                                            <strong>{{ $errors->first('image') }}</strong>
                                        @endif
                                    </div>
                                    <div class="form-group row">

                                        <input id="name" type="text"
                                               class="input-field @error('name') is-invalid @enderror"
                                               name="name" value="{{ old('name') }}" autocomplete="off"
                                               autofocus>
                                        <label for="name">{{ __('Name') }}</label>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <button type="submit" class="btn-form btn mx-auto d-flex text-center mt-5">UPDATE
                                        PROFILE
                                    </button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    import CancelButton from "../../js/components/CancelButton";

    export default {
        components: {CancelButton}
    }
</script>