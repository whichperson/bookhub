<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request)
    {
        $search = $request->get('keywords');
        $posts = DB::table('posts')->where('title', 'like', '%' . $search . '%')->where('user_id', '=', auth()->user()->id)->groupBy('title')->orderBy('created_at', 'DESC')->get();
        return response()->json($posts);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $data = request()->validate([
            'title' => 'required',
            'author' => 'required',
            'category' => 'required',
            'image' => ['required', 'image'],
        ]);

        $imagePath = request('image')->store('uploads', 's3');


        auth()->user()->posts()->create([
                'title' => $data['title'],
                'author' => $data['author'],
                'category' => $data['category'],
                'image' => $imagePath,
            ]

        );

        return redirect('/profile/' . auth()->user()->id);
    }


    public function show(\App\Post $post)
    {
        return view('posts.show', compact('post'));

    }

}
