<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(User $user)
    {
        return redirect("/profile/" . auth()->user()->id);
    }

    public function index(\App\User $user)
    {
        $postCount = Cache::remember(
            'count.posts.' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return $user->posts->count();
            });

        return view('profiles.index', compact('user', 'postCount'));
    }

    public function edit(\App\User $user)
    {
        $this->authorize('update', $user->profile);
        return view('profiles.index', compact('user'));
    }

    public function update(User $user)
    {
        $this->authorize('update', $user->profile);
        $data = request()->validate([
            'name' => 'required',
            'image' => '',
        ]);
        if (request('image')) {
            $imagePath = request('image')->store('profile', 's3');
            $imageArray = ['image' => $imagePath];
        }
        auth()->user()->profile->update(array_merge(
            $data,
            $imageArray ?? []
        ));
        return redirect("/profile/{$user->id}");
    }

    public function categories(User $user)
    {
        $posts = DB::table('posts')->where('user_id', '=', $user->id)->select('category', DB::raw('count(category) as count'))->groupBy('category')->orderBy('count', 'desc')->take(3)->get();
        return $posts->toJson(JSON_PRETTY_PRINT);

    }

    public function authors(User $user)
    {
        $posts = DB::table('posts')->where('user_id', '=', $user->id)->select('author', DB::raw('count(author) as count'))->groupBy('author')->orderBy('count', 'desc')->take(3)->get();
        return $posts->toJson(JSON_PRETTY_PRINT);

    }

}
