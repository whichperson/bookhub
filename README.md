# Bookhub 📚
[![Open Source Love svg2](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

Bookhub is your online library. You can keep track of your books and get a little more insight on your reading habits. 

## Why make this?
This was a personal project purely bred out of my own curiosity and need. As an avid reader I like to see my collection grow and be able to manage it. So I decided to create this web app and run it locally. Now, I don't have to log each book into my Excel sheet. I just upload it. 

## Features 🔥
There are a few key feautures that I focused mostly on. In the future, I plan on extending it and adding new functionalities.
*  User Register/Login System 
*  Upload books (Title, Author, Category, Cover)
* Search Bar: In case you forget which books you have just search and find. 
* Stat Charts: Most Read Categories and Favourite Authors. These are updated based on your uploads. 
* User Profile: You can edit your name and profile picture any time. 

## Technical Info ☕
The API endpoints follow a RESTful architecture as per Laravel's documentation. 
For example, to retrieve a user's most read categories: `/profile/{user}/categories` where `{user}` is the User model.

### Built with 🔨
- Laravel (PHP framework)
- Vue.js and some vanilla JavaScript
- HTML5 & Sass
- Amazon S3 (only for demo)

### Live Demo
This is deployed on Heroku for testing and previewing purposes. Some functionality only works locally (search, update profile picture). To fully experience the app, please download and run it locally. 
* Visit [bookhub-demo](https://bookhub-demo.herokuapp.com/)
* Sign up and test it

#### Instructions to run locally
1. Install PHP, Laravel, Node.js, npm, composer
2. `composer install` or `composer update`
3. `php artisan migrate:fresh`
4. `php artisan serve`

### Screenshots
![Sign In](https://i.imgur.com/fQfEwfi.png)
![Dashboard](https://i.imgur.com/2FSO56X.png)
![Search](https://i.imgur.com/PVINDo5.png)
![Profile Settings](https://i.imgur.com/keyZonN.png)

## License
MIT Licensed © Meropi L. 2019
You are free to clone/modify this repository with proper credit.