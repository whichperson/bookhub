const inputList = document.querySelectorAll(".input-field");
inputList.forEach(function (elem) {
    const icon = elem.previousElementSibling;
    const label = elem.nextElementSibling;

    elem.addEventListener('keydown', (e) => {
        if (elem.classList.contains('validate')) {
            if (elem.checkValidity()) {
                elem.classList.add('valid');
                icon.classList.add('valid');
                label.classList.add('valid');
                elem.classList.remove('invalid');
                icon.classList.remove('invalid');
                label.classList.remove('invalid');
            } else {
                elem.classList.add('invalid');
                icon.classList.add('invalid');
                label.classList.add('invalid');
                elem.classList.remove('valid');
                icon.classList.remove('valid');
                label.classList.remove('valid');
            }

            if (elem.value === "") {
                elem.classList.remove('valid');
                icon.classList.remove('valid');
                label.classList.remove('valid');
                elem.classList.remove('invalid');
                icon.classList.remove('invalid');
                label.classList.remove('invalid');
            }
        }

    });


    elem.addEventListener('focus', (e) => {
        icon.classList.add('active');
        label.classList.add('active');

        if (elem.classList.contains('validate')) {
            if (elem.value !== null) {
                if (elem.checkValidity()) {
                    elem.classList.add('valid');
                    icon.classList.add('valid');
                    label.classList.add('valid');
                } else {
                    elem.classList.remove('valid');
                    elem.classList.add('invalid');
                    icon.classList.remove('valid');
                    icon.classList.add('invalid');
                }
            }

            if (elem.value === "") {
                elem.classList.remove('valid');
                icon.classList.remove('valid');
                elem.classList.remove('invalid');
                icon.classList.remove('invalid');
            }
        }

    });

    elem.addEventListener('blur', (e) => {
        icon.classList.remove('active');
        if (elem.value === "") {
            label.classList.remove('active');
            label.classList.remove('valid');
            label.classList.remove('invalid');
        }
        if (elem.classList.contains('validate')) {

            if (elem.value !== null) {
                elem.classList.remove('valid');
                icon.classList.remove('valid');
                elem.classList.remove('invalid');
                icon.classList.remove('invalid');
            }
        }
    });
});

const showPasswordIcon = document.querySelector('.showPassword');
const hidePasswordIcon = document.querySelector('.hidePassword');
const input = document.querySelector('.password-field');

showPasswordIcon.addEventListener('click', () => {
    showPasswordIcon.classList.add('hide');
    hidePasswordIcon.classList.add('show');
    input.type = 'text';
});

hidePasswordIcon.addEventListener('click', () => {
    hidePasswordIcon.classList.remove('show');
    showPasswordIcon.classList.remove('hide');
    hidePasswordIcon.classList.add('hide');
    input.type = 'password';
});